/**
 * Created by phangty on 26/9/16.
 */
// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var UserSchema = new mongoose.Schema({
    userImagePath: {
        type: String,
        unique: true,
        required: true
    },
    roomName: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    LoggedDate: {
        type: Date,
        required: true
    }
}, {
    timestamps: true
});


// Export the Mongoose model
module.exports = mongoose.model('users', UserSchema);
