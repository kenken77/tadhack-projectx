var express = require("express");

var app = express();

var bodyParser = require("body-parser");
var session = require("express-session");
var cookieParser = require('cookie-parser');

var moment = require('moment-timezone');
var exphbs  = require('express-handlebars');
var mongoose = require('mongoose');

//var User = require('./user');

const  PORT = "port";
const SERVER_TZ = "Asia/Singapore";

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false,limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/tadhackproject-x');


app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Initialize session
app.use(session({
    secret: "tadhack-projectx",
    resave: false,
    saveUninitialized: true
}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

require("./routes.js")(app);

app.use(express.static(__dirname + "/../client"));

app.set(PORT, process.env.PORT || process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get(PORT) , function(){
    console.info("App Server started on " + app.get(PORT));
});
