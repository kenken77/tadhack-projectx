/**
 * Created by phangty on 15/10/16.
 */

var multer = require("multer");
var fs = require("fs");
var atob = require('atob');
var Users = require('./models/users');
var faceMatcher = require("./api/face-match/face-match.service")
var base64 = require('node-base64-image');

var randomstring = require("randomstring");
var Persistence  = require("./Persistence");
var User = require("./userImages");
const version = "/v1/api";
const REGISTRATION = "/register";
const LOGIN = "/login";

const IMG_DIRECTORY = "/img/";
const OK_IMG_DIRECTORY = __dirname + IMG_DIRECTORY + "/ok/";
const REGISTER_IMG_DIRECTORY = __dirname + IMG_DIRECTORY + "/register/";
const LOGIN_IMG_DIRECTORY = __dirname + IMG_DIRECTORY + "/login/";

const APP_KEY = "780e49e7-ebb1-494e-82a3-0559cfc4b7e8";

module.exports = function(app) {

    function generateRoomName(){
        return randomstring.generate(10);
    }
/*
    app.get(version + REGISTRATION + "/room-name", function (req, res) {
        console.log("--- registration get room name--- ");

        // pc belongs to room: 1
        // ken & ken belongs to room: 2
        // { room: ''}

    });

    app.get(version + LOGIN + "/room-name", function (req, res) {
        console.log("--- login get room name ---");

    });

    app.get(version + "/logout/:room_name", function (req, res) {
        console.log("---  logout --- ");
        console.info(req.params.room_name);
        // clear room from redis.
    });
*/
    /**
     * try something out !
     *//*
    app.get(version + "/users/", function (req, res) {
        console.log("---  logout --- ");
        console.info(req.params.room_name);
        Persistence.getAllUser(function(results){
            results.data.inspect =
                function(){ return returnResults(JSON.stringify( this, null, ' ' ),res); }
        });

    });*/

    app.post(version + REGISTRATION + "/upload", function (req, res) {
        console.log("--- registration ----  upload");
        var users = [];

        if(req.body.image && req.body.image.length){
            console.log("Do something with image");

            var base64Data = req.body.image.replace(/^data:image\/png;base64,/, "");
            var randFilename = randomstring.generate(10) +  ".png";
            console.log(REGISTER_IMG_DIRECTORY);
            fs.writeFile(REGISTER_IMG_DIRECTORY + randFilename, base64Data, 'base64', function(err) {
                if(err){
                    console.log(err);
                }
            });

            Persistence.getAllUser(function(results){
                //console.log(results);
                if (!results){
                    console.log("New !");
                    var usersArr = [];
                    var yy =  {
                        filename: randFilename,
                        base64Img: base64Data
                    };
                    usersArr.push(yy);
                    Persistence.saveUsers(usersArr);
                }else{
                    console.log("Append !");
                    //console.log(results.data);
                    var yy =  {
                        filename: randFilename,
                        base64Img: base64Data
                    };
                    var usersArr2 = [];
                    if(results.data instanceof Array) {
                        var value1 = results.data;
                        value1.forEach(function(data){
                            usersArr2.push(data);
                        })
                        usersArr2.push(yy);
                    }else{
                        var value2 = results.data;
                        usersArr2.push(value2);
                        usersArr2.push(yy);
                    }
                    Persistence.saveUsers(usersArr2);
                }
                var roomName = generateRoomName();
                console.log(roomName);
                faceMatcher.match(randFilename , function (userId) {
                    var user = new Users({
                        userImagePath: randFilename,
                        roomName: roomName,
                        LoggedDate: new Date(),
                        firstName: "Kenneth",
                        lastName: "Phang"
                     });
                    console.log(userId);
                    if(userId == null){
                        console.log(userId);
                        console.log("xxx");
                        user.save(function(err, userAfterCreated) {
                            if (err)
                                res.status(404).send('{error: "creating user!"}');

                            var successfulUser = {
                                message: 'New user created !',
                                userId:userAfterCreated._id,
                                fmUserId: userId,
                                appKey: APP_KEY,
                                roomName: roomName

                            };
                            returnResults(successfulUser, res);
                        });

                    }else{
                        console.log(userId);
                        console.log("Found you !");
                        Users.find({fmUserId: userId}, function(error, user){
                            if(error)
                                res.status(404).send(error);

                            var existingUser = {
                                message: 'User already exist!'
                                //appKey: APP_KEY,
                                //roomName: user.roomName

                            };
                            res.status(404).send(existingUser);
                            //returnResults(existingUser, res);
                        })

                    }
                });


            });
        }
        //
    });


    app.post(version + LOGIN + "/upload", function (req, res) {
        console.log("--- login ----  upload");
        if(req.body.image && req.body.image.length){
            console.log("Do something with image");
            var base64Data = req.body.image.replace(/^data:image\/png;base64,/, "");
            var randFilename = randomstring.generate(10) +  ".png";
            console.log(LOGIN_IMG_DIRECTORY);
            fs.writeFile(LOGIN_IMG_DIRECTORY + randFilename, base64Data, 'base64', function(err) {
                if(err){
                    console.log(err);
                }
            });

            faceMatcher.match(randFilename, function (userId) {
                if (userId) {
                    res.status(404).send('{error: "user not found!"}');
                } else {
                    Users.find({fmUserId: userId}, function(error, user) {
                        var userFound = {
                            message: 'User found !',
                            userId: user._id,
                            fmUserId: userId,
                            appKey: APP_KEY,
                            roomName: user.roomName

                        };
                        returnResults(userFound, res);
                    });
                }

            });

        }
    });

    app.get('/test', function (req, res) {
        res.render('test');
    });

    function returnResults(results, res) {
        console.log(results);
        res.status(200).send(results);
    }

}