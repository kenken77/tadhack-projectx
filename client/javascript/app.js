angular
    .module('MyApp', ['ui.router'])
    .config(AppStates);

AppStates.$inject = ['$urlRouterProvider', '$stateProvider'];

function AppStates($urlRouterProvider, $stateProvider) {

    $stateProvider
        .state('welcome', {
            url: '/welcome',
            templateUrl: 'views/welcome.html',
            controller: 'WelcomeCtrl',
            controllerAs: 'ctrl'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'ctrl'
        })
        .state('register', {
            url: '/register',
            templateUrl: 'views/register.html',
            controller: 'RegisterCtrl',
            controllerAs: 'ctrl'
        });
    $urlRouterProvider.otherwise("/welcome");
}


angular
    .module('MyApp')
    .controller('WelcomeCtrl', WelcomeCtrl);
WelcomeCtrl.$inject = ['$state', '$interval'];
function WelcomeCtrl($state, $interval) {
    var vm = this;
    vm.goLogin = function () {
        $state.go('login');
    };
    vm.goRegister = function () {
        $state.go('register');
    };

    var bar = document.getElementById('progress');
    bar.style.transition = 'all 0.1s';
    $interval(function () {
        var c = authcvs.acceptCounter * 100;
        bar.style.width = c + '%';
        bar.style.backgroundColor = 'rgba(' + (200 - c) + ',50,' + c + ',1)';
        if (authcvs.processed) {
            vm.goLogin();
        }
    }, 100);

    var skylink = new Skylink();

    var vid = document.getElementById('my-video');
    skylink.getUserMedia(function (error, success) {
        if (error) return;
        vid.autoplay = true;
        vid.muted = true;
        attachMediaStream(vid, success);
        authcvs.init(vid);
    });
}

angular
    .module('MyApp')
    .controller('LoginCtrl', LoginCtrl);
LoginCtrl.$inject = ['$state'];
function LoginCtrl($state) {
    var vm = this;

}

angular
    .module('MyApp')
    .controller('RegisterCtrl', RegisterCtrl);
RegisterCtrl.$inject = ['$state'];
function RegisterCtrl($state) {
    var vm = this;

}