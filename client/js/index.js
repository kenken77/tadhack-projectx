/**
 * Created by phangty on 15/10/16.
 */
var imgElem = document.getElementById('img');
imgElem.crossOrigin = "Anonymous";
$('#urlText').keyup(function () {
    $('#img').attr('src', $('#urlText').val());
});

$('#sendData').click(function () {
    var imgData = {
        image: getImage(imgElem)
    };
    $.ajax({
        url: '/v1/api/register/upload',
        //dataType: 'json',
        data: imgData,
        type: 'POST',
        success: function (data) {
            console.log(data);
        }
    });
});

function getImage(imgElem) {
// imgElem must be on the same server otherwise a cross-origin error will be thrown "SECURITY_ERR: DOM Exception 18"
    var canvas = document.createElement("canvas");
    canvas.width = imgElem.clientWidth;
    canvas.height = imgElem.clientHeight;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(imgElem, 0, 0);
    return canvas.toDataURL("image/jpg");
}